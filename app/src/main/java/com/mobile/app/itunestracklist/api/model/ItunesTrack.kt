package com.mobile.app.itunestracklist.api.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItunesTrack(
    val trackTitle: String,
    val trackGenre: String,
    val trackPrice: String,
    val trackArtwork: String,
    val trackDesc: String,
    val trackCurrency: String
) : Parcelable
