package com.mobile.app.itunestracklist.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.mobile.app.itunestracklist.db.dao.TrackDAO
import com.mobile.app.itunestracklist.db.entity.TrackEntity

@Database(
    entities = [TrackEntity::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun trackDao(): TrackDAO
}