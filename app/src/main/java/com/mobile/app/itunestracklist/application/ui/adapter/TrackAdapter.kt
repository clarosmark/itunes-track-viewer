package com.mobile.app.itunestracklist.application.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobile.app.itunestracklist.R
import com.mobile.app.itunestracklist.api.model.ItunesTrack
import com.mobile.app.itunestracklist.application.ui.viewholder.TrackViewHolder
import com.squareup.picasso.Picasso

class TrackAdapter constructor(
    private val context: Context,
    private val trackList: List<ItunesTrack>
) : RecyclerView.Adapter<TrackViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrackViewHolder {
        return TrackViewHolder(
            LayoutInflater.from(context).inflate(R.layout.layout_track_viewholder, parent, false)
        )
    }

    override fun getItemCount(): Int = trackList.size

    override fun onBindViewHolder(holder: TrackViewHolder, position: Int) {
        with(holder) {
            val title = trackList[position].trackTitle
            val trackGenre = trackList[position].trackGenre
            val trackPrice =
                trackList[position].trackCurrency + " " + trackList[position].trackPrice
            
            tvTrackName.text = "Title: $title"
            tvTrackGenre.text = "Genre: $trackGenre"
            tvTrackPrice.text = "Price: $trackPrice"

            Picasso.get()
                .load(trackList[position].trackArtwork)
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background)
                .into(imgTrack)
        }
    }

    fun getItem(position: Int): ItunesTrack = trackList[position]

}