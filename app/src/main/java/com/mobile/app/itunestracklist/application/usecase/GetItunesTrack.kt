package com.mobile.app.itunestracklist.application.usecase

import com.mobile.app.itunestracklist.api.ItunesService
import com.mobile.app.itunestracklist.api.model.ItunesTrack
import com.mobile.app.itunestracklist.db.dao.TrackDAO
import com.mobile.app.itunestracklist.db.entity.TrackEntity
import javax.inject.Inject

class GetItunesTrack @Inject constructor(
    private val itunesService: ItunesService,
    private val trackMapper: TrackMapper,
    private val trackDAO: TrackDAO
) {

    suspend fun getItunesTrack(): List<ItunesTrack> {
        val response = itunesService.getTrack()
        response.results.forEach { track ->
            val trackPrice = track.trackPrice ?: track.collectionPrice
            val trackName = track.trackName ?: track.collectionName
            val description = track.longDescription ?: track.description ?: ""
            val trackEntity = TrackEntity(
                uniqueID = track.trackId,
                trackImage = track.artworkUrl100,
                trackGenre = track.primaryGenreName,
                trackPrice = trackPrice,
                trackName = trackName,
                trackDesc = description,
                trackPriceCurrency = track.currency
            )
            trackDAO.insertTrack(trackEntity)
        }
        return getTrack()
    }

    fun getTrack(): List<ItunesTrack> {
        val data = arrayListOf<ItunesTrack>()
        val list = trackDAO.getItunesTrack()
        list.forEach { track ->
            data.add(
                trackMapper.mapToModel(track)
            )
        }
        return data
    }

    fun countLocalTrackRecord(): Int {
        return trackDAO.countTrack()
    }

}