package com.mobile.app.itunestracklist.application.ui

import android.view.GestureDetector
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.MotionEvent
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class RecyclerTouchListener constructor(
    recyclerView: RecyclerView,
    onItemClickListener: OnItemClickListener?,
    onItemLongClickListener: OnItemLongClickListener? = null
) : RecyclerView.OnItemTouchListener {

    private var gestureDetector: GestureDetector

    init {
        gestureDetector = GestureDetector(recyclerView.context,
            object : SimpleOnGestureListener() {
                override fun onSingleTapUp(e: MotionEvent): Boolean {
                    val child = recyclerView.findChildViewUnder(e.x, e.y)
                    if (child != null) {
                        onItemClickListener?.onClick(
                            child,
                            recyclerView.getChildAdapterPosition(child)
                        )
                    }
                    return true
                }

                override fun onLongPress(e: MotionEvent) {
                    val child = recyclerView.findChildViewUnder(e.x, e.y)
                    if (child != null) {
                        onItemLongClickListener?.onLongClick(
                            child,
                            recyclerView.getChildAdapterPosition(child)
                        )
                    }
                }
            })
        recyclerView.addOnItemTouchListener(this)
    }

    override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
        return gestureDetector.onTouchEvent(e)
    }

    override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
        //
    }

    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
        //
    }

    interface OnItemClickListener {
        fun onClick(view: View?, position: Int)
    }

    interface OnItemLongClickListener {
        fun onClick(view: View?, position: Int)
        fun onLongClick(view: View?, position: Int)
    }
}

