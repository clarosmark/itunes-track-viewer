package com.mobile.app.itunestracklist.application.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.mobile.app.itunestracklist.R
import com.mobile.app.itunestracklist.api.Constants
import com.mobile.app.itunestracklist.application.ui.RecyclerTouchListener
import com.mobile.app.itunestracklist.application.ui.adapter.TrackAdapter
import com.mobile.app.itunestracklist.application.ui.flow.TrackFlow
import com.mobile.app.itunestracklist.application.viewmodel.MainActivityViewModel
import com.mobile.app.itunestracklist.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel: MainActivityViewModel by viewModels()
    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: TrackAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.recyclerView.layoutManager = LinearLayoutManager(applicationContext)
        binding.recyclerView.itemAnimator = DefaultItemAnimator()
        val touchListener = RecyclerTouchListener(
            binding.recyclerView,
            object : RecyclerTouchListener.OnItemClickListener {
                override fun onClick(view: View?, position: Int) {
                    val adapter = binding.recyclerView.adapter as TrackAdapter
                    val data = adapter.getItem(position)
                    val intent = Intent(this@MainActivity, TrackDetailsActivity::class.java).apply {
                        putExtra(Constants.TRACK_DETAILS, data)
                    }
                    startActivity(intent)
                }
            })
        binding.recyclerView.addOnItemTouchListener(touchListener)

        lifecycleScope.launchWhenStarted {
            viewModel.trackFlow.collect {
                when (it) {
                    is TrackFlow.Loading -> {
                        binding.formContainer.visibility =
                            if (it.isLoading) View.GONE else View.VISIBLE
                        binding.progressBar.visibility =
                            if (it.isLoading) View.VISIBLE else View.GONE
                    }
                    is TrackFlow.Success -> {
                        adapter = TrackAdapter(applicationContext, it.trackList)
                        binding.recyclerView.adapter = adapter
                    }
                    is TrackFlow.Error -> {
                        showErrorDialog()
                    }
                    else -> {

                    }
                }
            }
        }

    }

    private fun showErrorDialog() {
        if (isFinishing) {
            return
        }
        val builder = AlertDialog.Builder(this).apply {
            setTitle(R.string.error_title)
            setMessage(R.string.error_message)
                .setNegativeButton(android.R.string.ok) { _, _ ->
                    finish()
                }
                .create()
        }
        builder.show()
    }

}