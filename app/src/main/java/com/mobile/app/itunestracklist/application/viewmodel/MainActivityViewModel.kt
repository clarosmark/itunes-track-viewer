package com.mobile.app.itunestracklist.application.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mobile.app.itunestracklist.application.ui.flow.TrackFlow
import com.mobile.app.itunestracklist.application.usecase.GetItunesTrack
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor(
    private val getItunesTrack: GetItunesTrack
) : ViewModel() {

    private val _trackFlow = MutableSharedFlow<TrackFlow>(replay = 3)
    val trackFlow: SharedFlow<TrackFlow> = _trackFlow

    init {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _trackFlow.emit(TrackFlow.Loading(true))
                val list = getItunesTrack.getItunesTrack()
                _trackFlow.emit(TrackFlow.Success(list))
            } catch (e: Throwable) {
                Timber.e(e)
                val countRecord = getItunesTrack.countLocalTrackRecord()
                if (countRecord > 0){
                    val data = getItunesTrack.getTrack()
                    _trackFlow.emit(TrackFlow.Success(data))
                }
                else{
                    //show dialog error

                }
            } finally {
                _trackFlow.emit(TrackFlow.Loading(false))
            }
        }
    }

}