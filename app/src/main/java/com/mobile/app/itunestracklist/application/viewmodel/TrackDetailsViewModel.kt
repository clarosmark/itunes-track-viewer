package com.mobile.app.itunestracklist.application.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mobile.app.itunestracklist.api.model.ItunesTrack
import com.mobile.app.itunestracklist.application.ui.flow.TrackDetailsFlow
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class TrackDetailsViewModel @Inject constructor() : ViewModel() {

    private lateinit var itunesTrack: ItunesTrack

    private val _detailsFlow = MutableSharedFlow<TrackDetailsFlow>()
    val detailsFlow: SharedFlow<TrackDetailsFlow> = _detailsFlow

    fun setData(itunesTrack: ItunesTrack) {
        this.itunesTrack = itunesTrack
        viewModelScope.launch(Dispatchers.Main) {
            val price = itunesTrack.trackCurrency + " " + itunesTrack.trackPrice
            _detailsFlow.emit(TrackDetailsFlow.TrackPrice(price))
            _detailsFlow.emit(TrackDetailsFlow.TrackName(itunesTrack.trackTitle))
            _detailsFlow.emit(TrackDetailsFlow.TrackDesc(itunesTrack.trackDesc))
            _detailsFlow.emit(TrackDetailsFlow.TrackGenre(itunesTrack.trackGenre))
            _detailsFlow.emit(TrackDetailsFlow.TrackImage(itunesTrack.trackArtwork))
        }
    }

}