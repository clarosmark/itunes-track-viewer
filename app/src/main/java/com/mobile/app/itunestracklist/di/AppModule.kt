package com.mobile.app.itunestracklist.di

import android.content.Context
import android.net.Uri
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.mobile.app.itunestracklist.ItunesTrackApplication
import com.mobile.app.itunestracklist.api.ItunesService
import com.mobile.app.itunestracklist.application.usecase.TrackMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.TlsVersion
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    private const val SCHEME: String = "https"
    private const val AUTHORITY: String = "itunes.apple.com"

    private val BASE_URI = Uri.Builder()
        .scheme(SCHEME)
        .encodedAuthority(AUTHORITY)
        .build()


    @Singleton
    @Provides
    fun provideApplication(@ApplicationContext context: Context): ItunesTrackApplication {
        return context as ItunesTrackApplication
    }

    @Singleton
    @Provides
    fun provideOkHttpBuilder(interceptor: HttpLoggingInterceptor): OkHttpClient.Builder {

        return OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(90, TimeUnit.SECONDS)
            .connectionSpecs(
                listOf(
                    ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                        .tlsVersions(TlsVersion.TLS_1_2)
                        .build(), ConnectionSpec.Builder(ConnectionSpec.CLEARTEXT)
                        .build()
                )
            )
    }


    @Singleton
    @Provides
    fun provideRetrofit(
        gson: Gson, okHttpBuilder: OkHttpClient.Builder,
        interceptor: HttpLoggingInterceptor,
//        errorInterceptor: ErrorInterceptor
    ): Retrofit {
        val builder = okHttpBuilder.addInterceptor(interceptor)
//            .addInterceptor(errorInterceptor)
        return Retrofit.Builder()
            .baseUrl(BASE_URI.toString())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(builder.build())
            .build()
    }

    @Singleton
    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().setLevel(
            HttpLoggingInterceptor.Level.BODY
        )
    }

    @Singleton
    @Provides
    fun provideMovieService(retrofit: Retrofit): ItunesService {
        return retrofit.create(ItunesService::class.java)
    }

    @Singleton
    @Provides
    fun provideGson(): Gson {
        return GsonBuilder()
//            .setExclusionStrategies(object : ExclusionStrategy {
//                override fun shouldSkipField(f: FieldAttributes): Boolean {
//                    return f.getAnnotation(SkipSerialisation::class.java) != null
//                }
//
//                override fun shouldSkipClass(clazz: Class<*>?): Boolean {
//                    return false
//                }
//            })
            .setLenient()
            .create()
    }

    @Provides
    fun provideTrackMapper(): TrackMapper = TrackMapper

}