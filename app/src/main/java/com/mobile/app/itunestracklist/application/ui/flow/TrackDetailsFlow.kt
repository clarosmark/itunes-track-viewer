package com.mobile.app.itunestracklist.application.ui.flow

sealed class TrackDetailsFlow {
    data class TrackImage(val trackImage: String) : TrackDetailsFlow()
    data class TrackName(val trackName: String) : TrackDetailsFlow()
    data class TrackDesc(val trackDesc: String) : TrackDetailsFlow()
    data class TrackPrice(val trackPrice: String) : TrackDetailsFlow()
    data class TrackGenre(val trackGenre: String) : TrackDetailsFlow()
    object Empty : TrackDetailsFlow()
}

