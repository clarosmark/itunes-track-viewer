package com.mobile.app.itunestracklist.application.activity

import android.os.Bundle
import android.text.Html
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.mobile.app.itunestracklist.R
import com.mobile.app.itunestracklist.api.Constants
import com.mobile.app.itunestracklist.api.model.ItunesTrack
import com.mobile.app.itunestracklist.application.ui.flow.TrackDetailsFlow
import com.mobile.app.itunestracklist.application.viewmodel.TrackDetailsViewModel
import com.mobile.app.itunestracklist.databinding.ActivityTrackDetailsBinding
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class TrackDetailsActivity : AppCompatActivity() {


    private val viewModel: TrackDetailsViewModel by viewModels()

    private lateinit var binding: ActivityTrackDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityTrackDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle = intent.extras
        val data = bundle?.getParcelable<ItunesTrack>(Constants.TRACK_DETAILS)
        data?.let {
            viewModel.setData(
                it
            )
        }

        lifecycleScope.launch {
            viewModel.detailsFlow.collect {
                when (it) {
                    is TrackDetailsFlow.TrackName -> {
                        binding.tvTrackName.text = it.trackName
                    }
                    is TrackDetailsFlow.TrackGenre -> {
                        binding.tvTrackGenre.text = it.trackGenre
                    }
                    is TrackDetailsFlow.TrackDesc -> {
                        binding.tvTrackDesc.text = "Description: " + Html.fromHtml(
                            it.trackDesc,
                            Html.FROM_HTML_MODE_LEGACY
                        )
                    }
                    is TrackDetailsFlow.TrackPrice -> {
                        binding.tvTrackPrice.text = it.trackPrice
                    }
                    is TrackDetailsFlow.TrackImage ->{
                        val image = it.trackImage
                        Picasso.get()
                            .load(image)
                            .placeholder(R.drawable.ic_launcher_background)
                            .error(R.drawable.ic_launcher_background)
                            .into(binding.imgView)
                    }
                    else -> {


                    }
                }
            }
        }

    }

}