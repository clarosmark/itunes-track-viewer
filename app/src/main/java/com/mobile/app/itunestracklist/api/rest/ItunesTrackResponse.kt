package com.mobile.app.itunestracklist.api.rest

data class ItunesTrackResponse(
    val resultCount: Int,
    val results: List<Result>
)