package com.mobile.app.itunestracklist.application.ui.flow

import com.mobile.app.itunestracklist.api.model.ItunesTrack

sealed class TrackFlow{
    data class Success(val trackList: List<ItunesTrack>) : TrackFlow()
    data class Error(val title: String, val errorMessage: String) : TrackFlow()
    data class Loading(val isLoading: Boolean) : TrackFlow()
    object Empty : TrackFlow()
}
