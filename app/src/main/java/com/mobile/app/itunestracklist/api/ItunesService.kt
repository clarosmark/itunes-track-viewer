package com.mobile.app.itunestracklist.api

import com.mobile.app.itunestracklist.api.rest.ItunesTrackResponse
import retrofit2.http.GET

interface ItunesService {

    @GET("/search?term=star&amp;amp;country=au&amp;amp;media=movie&amp;amp;all")
    suspend fun getTrack(): ItunesTrackResponse


}