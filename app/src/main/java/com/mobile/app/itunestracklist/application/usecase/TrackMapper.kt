package com.mobile.app.itunestracklist.application.usecase

import com.mobile.app.itunestracklist.api.model.ItunesTrack
import com.mobile.app.itunestracklist.db.entity.TrackEntity

object TrackMapper {

    fun mapToModel(trackEntity: TrackEntity) : ItunesTrack{
        return ItunesTrack(
            trackTitle = trackEntity.trackName,
            trackDesc = trackEntity.trackDesc,
            trackPrice = trackEntity.trackPrice.toString(),
            trackGenre = trackEntity.trackGenre,
            trackArtwork = trackEntity.trackImage,
            trackCurrency = trackEntity.trackPriceCurrency
        )
    }

//    fun mapToEntity(trackResponse: List<Result>) : TrackEntity{
//
//    }

}