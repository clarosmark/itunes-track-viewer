package com.mobile.app.itunestracklist.db.dao

import androidx.room.*
import com.mobile.app.itunestracklist.db.entity.TrackEntity

@Dao
interface TrackDAO {

    @Query("SELECT * from itunes_track")
    fun getItunesTrack(): List<TrackEntity>

    @Query("SELECT COUNT(uniqueID) from itunes_track")
    fun countTrack(): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTrack(vararg trackEntity: TrackEntity)

    @Delete
    fun deleteTrack(vararg trackEntity: TrackEntity)


}