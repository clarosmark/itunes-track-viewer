package com.mobile.app.itunestracklist.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "itunes_track")
data class TrackEntity(
    @PrimaryKey
    val uniqueID: Int,
    @ColumnInfo(name = "track_name") val trackName: String,
    @ColumnInfo(name = "track_genre") val trackGenre: String,
    @ColumnInfo(name = "track_price") val trackPrice: Double,
    @ColumnInfo(name = "track_desc") val trackDesc: String,
    @ColumnInfo(name = "track_price_currency") val trackPriceCurrency: String,
    @ColumnInfo(name = "track_artwork") val trackImage: String,
)