package com.mobile.app.itunestracklist.application.ui.viewholder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mobile.app.itunestracklist.R

class TrackViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val tvTrackName: TextView
        get() = itemView.findViewById(R.id.tv_track_name)

    val tvTrackGenre: TextView
        get() = itemView.findViewById(R.id.tv_track_genre)

    val tvTrackPrice: TextView
        get() = itemView.findViewById(R.id.tv_track_price)

    val imgTrack: ImageView
        get() = itemView.findViewById(R.id.img_view)

}