# Itunes Track Viewer

This project was created in Kotlin and the used the Android MVVM Architecture.


## Documentation version

Implementing an Itunes track viewer wherein it has descriptions, title, image and the price of each track.

### It has 2 Features:
- List of tracks
- Track Details
- (Soon) add preview to the tracks and redirect to Itunes

### Technologies that was used: 
- Kotlin
- Coroutines for management of heavy process
- Room database (Supports offline load of data as long it's loaded for the first time)
- Instead of using LiveData in MVVM Architecture, I used coroutines lifecycle: A job handling all of the threads

## Installation

Please download the app [here](https://drive.google.com/drive/folders/1c7wuVjx4OeZUXk67KoIa_1kua0EooZGU?usp=sharing)


